import {Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  showForm = false;
  url = '';
  title = '';
  password = '';
  image = [
    {title: 'car', url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcST2T3NMQwKkMlmhr6Zx6jZMMalPSV3xtIztxcLOXawFu3xXEJlqy53TocVE7voUOBhfDs&usqp=CAU'},
    {title: 'car', url: 'https://s7g10.scene7.com/is/image/maserati/maserati/international/Models/mc20/mc20-hero.jpg?$600x2000$&fit=constrain'},
    {title: 'car', url: 'https://cdn.motor1.com/images/mgl/8e8Mo/s1/most-expensive-new-cars-ever.webp'},
  ];

  openForm(event: Event) {
    event.preventDefault();
    this.showForm = this.password === 'wsx';
  }

  onAddImage(event: Event) {
    event.preventDefault();
    this.image.push({
      title: this.title,
      url: this.url,
    });
    this.showForm = false;
  }
}
