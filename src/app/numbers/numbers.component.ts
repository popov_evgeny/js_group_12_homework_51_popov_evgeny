import {Component} from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  numbers: number [];

  constructor() {
    this.numbers = [];
  }

  generateNumbers() {
    this.numbers = [];
    for (let i = 0; i <= 4; i++){
      this.numbers.push(Math.floor(Math.random() * (36 - 5)) + 5);
    }
    return this.numbers.sort(function (a,b){return a-b});
  }

}
