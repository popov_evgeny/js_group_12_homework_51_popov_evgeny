import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent {
  @Input() url = 'https://s7g10.scene7.com/is/image/maserati/maserati/international/Models/mc20/mc20-hero.jpg?$600x2000$&fit=constrain';
  @Input() title = 'car';
}
